# Vue Login

## Descripción

Crear una forma de "login" en VueJS, la forma debe tener un campo de usuario y de contraseña. Debe conectarse al [AUTH API](https://gitlab.com/leackstat/auth_api) creado en Laravel para validar al usuario.
